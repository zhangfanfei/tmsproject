/****   request.js   ****/
import {delCookie, getCookie} from "./util";
import store from '../store/store';
/*动画*/
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 导入axios
import axios from 'axios'
// 使用element-ui Message做消息提醒
import {Message} from 'element-ui';
// 跳转登录界面
import router from "../router/router";

//1. 创建新的axios实例，
const service = axios.create({
  // 公共接口--这里注意后面会讲
  baseURL: '/QHangApi',
  // 超时时间 单位是ms，这里设置了10s的超时时间
  timeout: 20 * 1000
})
// 2.请求拦截器
service.interceptors.request.use(config => {
  NProgress.start()
  //发请求前做的一些处理，数据转化，配置请求头，设置token,设置loading等，根据需求去添加
  config.data = JSON.stringify(config.data); //数据转化,也可以使用qs转换
  config.headers = {
    'Content-Type': 'application/json' //配置请求头
  }
  config.headers.token = getCookie('token'); //获取token把token放入头
  return config
}, error => {
  Promise.reject(error)
})

// 3.响应拦截器
service.interceptors.response.use(response => {
  NProgress.done()
  store.commit("startAnimation", false); // 停止动画(防止报错动画不停止)
  let code = response.data.code // 状态码
  switch (code) {
    case 201:
      response.message = response.data.message
      break;
    case 203:
      router.replace({path: '/noAccess'})
      break;
    case 204:
      response.message = response.data.message
      break;
    case 206:
      response.message = response.data.message + "，请重新登录！！！"
      setTimeout(() => {
        //重新登录
        router.replace({path: '/login'}).then(() => {
          // 清空token
          delCookie('token')
        })
      }, 1500)
      break;
  }
  if (response.message) {
    // 打印
    Message.error(response.message)
  }
  return response
}, error => {
  store.commit("startAnimation", false); // 停止动画(防止报错动画不停止)
  if (error && error.response) {
    // 1.公共错误处理
    // 2.根据响应码具体处理
    switch (error.response.status) {
      case 400:
        error.message = '错误请求'
        break;
      case 401:
        error.message = '未授权，请重新登录'
        break;
      case 403:
        error.message = '拒绝访问'
        break;
      case 404:
        error.message = '请求错误,未找到该资源'
        break;
      case 405:
        error.message = '请求方法未允许'
        break;
      case 408:
        error.message = '请求超时'
        break;
      case 500:
        error.message = '服务器端出错'
        break;
      case 501:
        error.message = '网络未实现'
        break;
      case 502:
        error.message = '网络错误'
        break;
      case 503:
        error.message = '服务不可用'
        break;
      case 504:
        error.message = '网络超时'
        break;
      case 505:
        error.message = 'http版本不支持该请求'
        break;
      default:
        error.message = `连接错误${error.response.status}`
    }
  } else {
    // 超时处理
    if (JSON.stringify(error).includes('timeout')) {
      Message.error('服务器响应超时，请刷新当前页')
      return Promise.resolve(error.response)
    }
    error.message = '连接服务器失败'
  }
  if (error.message) {
    // 打印
    Message.error(error.message)
  }
  return Promise.resolve(error.response)
})
//4.导出文件
export default service
