// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex'
// 引入element UI
import ElementUI, {Message} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/base.css'; // 动画样式
import './QHangCss.css'
import App from './App';
// 引入路由
import router from './router/router';
// 引入状态管理
import store from './store/store';
// 引入icon
import './assets/icon/iconfont.css'
// 引入echarts
import axios from 'axios';
// 过滤器
import * as custom from './utils/util'

import BaiduMap from "vue-baidu-map";
Vue.use(BaiduMap, {
  ak: 'HQ7GzsIr9jkNWsFY6fMOsChIW3NSMnRk'
})

Vue.prototype.$axios = axios;
Vue.config.productionTip = false;
// 使用element UI
Vue.use(ElementUI);
Vue.use(Vuex)

Object.keys(custom).forEach(key => {
  Vue.filter(key, custom[key])
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store, //使用store vuex状态管理
  components: {
    App
  },
  template: '<App/>',
  data: {
    // 空的实例放到根组件下，所有的子组件都能调用
    Bus: new Vue()
  }
})

// 路由拦截器
router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    next();
  } else {// 每次页面跳转执行，token錯誤，均提示跳转到首页
    let token = custom.getCookie('token');
    if (token === null || token === '' || token === undefined) {
      Message.error('用户令牌不存在！！！')
      setTimeout(function () {
        next('/login');
      }, 1000)
    } else {
      if (to.name === null) {
        // 路由不存在
        next('/noPage');
      } else {
        next();
      }
    }
  }
});
