import http from '../../../utils/http'

export default {
  // 查询详情
  getObj:(id) => http.get(`/api/administratorControllerServer/pdCourierScop/getCourierScop/${id}`),

  updateCourierScop:(id,list) => http.post(`/api/administratorControllerServer/pdCourierScop/insertOrUpdatePdCourierScop/${id}`,list)
}
