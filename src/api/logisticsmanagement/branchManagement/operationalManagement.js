import http from '../../../utils/http'

export default {
  // 分页查询
  getPdTaskPickupDispatchPageFind:(page,size,pdTaskPickupDispatch) => http.post(`/api/administratorControllerServer/pdTaskPickupDispatch/getPdTaskPickupDispatchPageFind/${page}/${size}`, pdTaskPickupDispatch)
}
