import http from '../../../utils/http'

export default {
  // 分页查询
  getPage:(page,size,obj) => http.post(`/api/administratorControllerServer/pdTaskPickupDispatch/getPagePdAuthUser/${page}/${size}`, obj)
}
