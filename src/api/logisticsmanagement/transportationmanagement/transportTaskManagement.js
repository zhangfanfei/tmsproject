import http from "../../../utils/http";

let httpHeadr = '/api/administratorControllerServer/transportTask'
export default ({
  getPdTaskTransportData: (param) => http.post(`${httpHeadr}/getPage`, param),

  getTruckOrderData: (param) => http.get(`${httpHeadr}/selTransportOrder/${param}`)
})
