import http from "../../../utils/http";

export default ({
  getdriverData: (params) => http.post(`/api/administratorControllerServer/driverJobManagementFeignController/getPdTaskTransportPage`, params)
})
