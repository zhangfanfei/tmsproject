import http from '../../../utils/http'

let httpHeadr = '/api/administratorControllerServer/pgGoodsTyps'
export default {
  // 添加
  insertGoodsData: (param) => http.post(`${httpHeadr}/addPdGoodsType`, param),
  // 查询修改信息
  selUpdataGoodsData: (param) => http.get(`${httpHeadr}/selUpdataGoodsInfo/${param}`),
  // 删除
  deleGoodsData: (param) => http.delete(`${httpHeadr}/delPdGoodsType/${param}`, ),
  // 查询车辆
  getTruck: (param) => http.get(`${httpHeadr}/selPdTruckTypeAll`, param),
  // 修改
  updateGoodsData: (param) => http.put(`${httpHeadr}/updateGoodsInfo`, param),
  // 查询
  getGoodsData: (param) => http.post(`${httpHeadr}/selGoodsType`, param),
}
