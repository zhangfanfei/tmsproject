import method from "../../../utils/http";

let url = '/api/administratorControllerServer'
export default {
  //订单分页
  selectOrder: (page, size, order) => method.post(`${url}/pdOrder/getPdOrderPage/${page}/${size}`, order),
  //行政区树数据
  getPdAreaTree: () => method.get('/api/userControllerServer/pdArea/getPdAreaTree'),
  // 详情
  getOrderXQ: (id) => method.get(`${url}/pdOrder/getPdOrder/${id}`),
  // 获取取件派件信息
  getDelivery: (id, type) => method.get(`${url}/pdTaskPickupDispatch/getTPDByTaskTypeByOId/${id}/${type}`),
  // 使用地址获取经纬度
  getAddress: (add) => method.get(`${url}/pdOrder/getCoordinates/${add}`),
  // 获取运输详情
  getTruckOrderInfo: (id) => method.get(`${url}/transportTask/getShippingInformationByWaybillId/${id}`),
  // 根据组织id查询快递员
  getCourierByOrgId: (agencyId) => method.get(`${url}/pdTaskPickupDispatch/getListPdAuthUserAnCourier/${agencyId}`),
  // 快递员分配确认
  setCourier: (params) => method.post(`${url}/pdTaskPickupDispatch/okTheOneCourierGetTask`, params)
}
