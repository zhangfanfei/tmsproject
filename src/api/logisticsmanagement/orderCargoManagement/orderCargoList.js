import method from "../../../utils/http";

export default {
  //订单分页
  selectOrderCargo: (page,size,order) => method.post(`/api/administratorControllerServer/pdTransportOrder/getPagePdTransportOrder/${page}/${size}`, order)
}
