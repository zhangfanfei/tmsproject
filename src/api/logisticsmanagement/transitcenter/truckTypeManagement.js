import http from '../../../utils/http'

export default {
    //  分页查询车辆类型及相关数据
    selTruckTypeAll:(pagenum,pagesize,queryVO) => http.post(`/api/administratorControllerServer/pdTruckType/truckgoodspage/${pagenum}/${pagesize}`,queryVO),

    //  查询所有货物类型
    selGoodsAll:() => http.get(`/api/administratorControllerServer/pdTruckType/findgoods`),

    //  根据车辆类型ID查询货物类型
    selGoodsByTruckTypeId:(truckid) => http.get(`/api/administratorControllerServer/pdTruckType/findgoodsbytruckid/${truckid}`),

    //  添加车辆类型
    insTruckType:(truckType) => http.post(`/api/administratorControllerServer/pdTruckType/instrucktype`,truckType),

    //  修改车辆类型
    updTruckType:(truckType) => http.put(`/api/administratorControllerServer/pdTruckType/updtruck`,truckType),

    //  删除车辆类型
    delTruckType:(truckTypeId) => http.delete(`/api/administratorControllerServer/pdTruckType/deltrucktype/${truckTypeId}`)
  }
