import http from '../../../utils/http'

export default {

  //  条件分页查询线路类型及相关数据
  pageLineType:(pagenum,pagesize,queryVO) => http.post(`/api/administratorControllerServer/pdTransportLineType/pagefindlinetype/${pagenum}/${pagesize}`,queryVO),

  //  添加线路类型数据
  addLineType:(addLineData) => http.post(`/api/administratorControllerServer/pdTransportLineType/inslinetype`,addLineData),

  //  根据线路类型ID删除线路类型数据
  delLineType:(linetypeid) => http.delete(`/api/administratorControllerServer/pdTransportLineType/dellinetype/${linetypeid}`),

  //  确定修改线路数据
  updLineType:(updLineData) => http.put(`/api/administratorControllerServer/pdTransportLineType/updfindlinetype`,updLineData)

}
