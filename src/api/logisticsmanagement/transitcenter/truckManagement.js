import http from '../../../utils/http'

export default {

    //  分页查询车辆及修改数据
    selTruckAll:(pagenum,pagesize,Truck) => http.post(`/api/administratorControllerServer/pdTruck/findtruck/${pagenum}/${pagesize}`,Truck),

    //  查询所有车辆类型
    selTruckTypeAll:() => http.get(`/api/administratorControllerServer/pgGoodsTyps/selPdTruckTypeAll`),

    //  查询所有车队
    selFleetAll:() => http.get(`/api/administratorControllerServer/pdTruckFleet/findfleetall`),

    //  添加车辆
    insTruck:(Truck) => http.post(`/api/administratorControllerServer/pdTruck/instruck`,Truck),

    //  删除车辆
    delTruck:(TruckId) => http.delete(`/api/administratorControllerServer/pdTruck/deltruck/${TruckId}`),

    //  查询车辆基本信息
    selTruckDetails:(TruckId) => http.get(`/api/administratorControllerServer/pdTruck/findtruckdetails/${TruckId}`),

    //  修改车辆信息
    updTruck:(updTruckData) => http.put(`/api/administratorControllerServer/pdTruck/updtruckdetails`,updTruckData),

    //  修改车辆行驶证信息
    updTruckLicense:(TruckLicense) => http.put(`/api/administratorControllerServer/pdTruck/updtrucklicense`,TruckLicense),

    //  车辆行驶证图片上传
    fileUpload:(license) => http.put(`/api/administratorControllerServer/pdTruck/updtruckfile`,license)
}
