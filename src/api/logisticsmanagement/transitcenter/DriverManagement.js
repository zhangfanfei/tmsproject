import http from '../../../utils/http'

export default {

    //  查询所有车队数据
    selTruckFleet:() => http.get(`/api/administratorControllerServer/pdTruckFleet/findfleetall`),

    //  查询所有司机
    pageTruckDriver:(pagenum,pagesize,fleetid,drivername) => http.get(`/api/administratorControllerServer/pdTruckFleet/pagetruckdriver/${pagenum}/${pagesize}/${fleetid}/${drivername}`),

    //  修改司机对应的车队
    updDriverFleet:(driverId,fleetId) => http.put(`/api/administratorControllerServer/pdTruckDriver/upddriverfleet/${driverId}/${fleetId}`),

    //  查询司机详情数据
    selDriverDetails:(DriverId) => http.get(`/api/administratorControllerServer/pdTruckDriver/finddriverdetails/${DriverId}`),

    //  修改司机的驾驶证
    updDriverLicense:(License) => http.put(`/api/administratorControllerServer/pdTruckDriver/upddriverlicense`,License),

    //  修改司机驾驶证图片
    fileUpload:(license) => http.put(`/api/administratorControllerServer/pdTruckDriver/fileUpload`,license)

}
