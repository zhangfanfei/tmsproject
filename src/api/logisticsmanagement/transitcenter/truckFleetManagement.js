import http from '../../../utils/http'

export default {

  //  条件分页查询车队及相关数据
  pageTruckFleet:(pagenum,pagesize,PdFleet) => http.post(`/api/administratorControllerServer/pdTruckFleet/fleetuserdrivertruck/${pagenum}/${pagesize}`,PdFleet),

  //  删除车队
  delfleet:(fleetId) => http.delete(`/api/administratorControllerServer/pdTruckFleet/delfleet/${fleetId}`),

  //  查询所有车队负责人
  selFleetPrincipal:() => http.get(`/api/administratorControllerServer/pdTruckFleet/findfleetprincipalall`),

  //  添加车队
  insFleet:(addFleetData) => http.post(`/api/administratorControllerServer/pdTruckFleet/insfleet`,addFleetData),

  //  修改车队
  updFleet:(updFleetData) => http.put(`/api/administratorControllerServer/pdTruckFleet/updfleet`,updFleetData)
}
