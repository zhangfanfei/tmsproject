import http from '../../../utils/http'

export default {

  //  查询所有线路类型
  selLineType:() => http.get(`/api/administratorControllerServer/pdTransportLineType/findlineall`),

  //  查询所有线路
  pageLine:(pagenum,pagesize,lineObj) => http.post(`api/administratorControllerServer/pdTransportLine/pagefindline/${pagenum}/${pagesize}`,lineObj),

  //  添加线路数据
  insLine:(addLineData) => http.post(`api/administratorControllerServer/pdTransportLine/insline`,addLineData),

  //  删除线路数据
  delLine:(id) => http.delete(`api/administratorControllerServer/pdTransportLine/delline/${id}`),

  //  修改线路数据
  updLine:(updLineData) => http.put(`api/administratorControllerServer/pdTransportLine/updline`,updLineData),

  //  添加车次
  addTrips:(dx) => http.post(`api/administratorControllerServer/pdTransportLine/insTrips`,dx),

  //  修改车次
  updTrips:(dx) => http.put(`api/administratorControllerServer/pdTransportLine/updtrips`,dx),

  //  删除车次
  delTrips:(id) => http.delete(`api/administratorControllerServer/pdTransportLine/delTrips/${id}`),

  //  查询所有车队和对应的车辆
  selFleetTruck:() => http.get(`api/administratorControllerServer/pdTransportLine/findfleettruckall`),

  //  查询所有未使用的车辆
  selTruckAll:() => http.post(`api/administratorControllerServer/pdTruck/findtruckall`),

  //  给车次添加车辆
  insTruck:(dx) => http.post(`api/administratorControllerServer/pdTransportLine/instripstruck`,dx),

  // 根据车次ID查询所有关联的车辆司机数据
  selTripsTruckDriver:(id) => http.post(`api/administratorControllerServer/pdTransportLine/findtripstrukcdriver/${id}`),

  //  查询树状车队车辆数据
  findFleetDriver:() => http.get(`api/administratorControllerServer/pdTransportLine/findfleetdriverall`),

  //  给车辆添加司机
  instripsdriver:(obj) => http.post(`api/administratorControllerServer/pdTransportLine/instripsdriver`,obj),

  //  删除车次里的车辆
  delTripsTruck:(id) => http.delete(`api/administratorControllerServer/pdTransportLine/deltripstruck/${id}`),
}
