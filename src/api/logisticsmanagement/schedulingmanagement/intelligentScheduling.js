import http from '../../../utils/http'

export default {

  //  根据机构ID查询定时任务
  findJobByOrgId:(id) => http.post(`/api/administratorControllerServer/scheduleJob/findjob/${id}`),


  //  修改机构的定时任务
  updJob:(dx)  => http.put(`/api/administratorControllerServer/scheduleJob/updjob`,dx),


  //  增加机构的定时任务
  insJob:(dx) => http.post(`/api/administratorControllerServer/scheduleJob/addjob`,dx),
}
