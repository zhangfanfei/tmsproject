import http from '../../../utils/http'

export default {

  //  分页查询组织下的定时任务日志
  pageFindJobLogByOrgId:(pagenum,pagesize,orgid) => http.post(`/api/administratorControllerServer/scheduleJobLog/pagefindjoblog/${pagenum}/${pagesize}/${orgid}`),
}
