import method from "../../../utils/http";

export default {
  //根据状态及条件查询所有活动组织
  queryCoreOrgAllInfo: (obj) => method.post(`/api/administratorControllerServer/organizationManagement/queryCoreOrgAllInfo`, obj),
  // 查询某个组织位置信息
  getAgencyScope: (id) => method.get(`/api/administratorControllerServer/pdAgencyScope/findOrgById/${id}`),
  // 修改或添加组织位置
  updateInsert: (id, obj) => method.post(`/api/administratorControllerServer/pdAgencyScope/updateOnInsert/${id}`, obj)
}
