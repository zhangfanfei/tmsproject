import http from '../../../utils/http'

export default {
  // 分页查询
  getPage:(obj) => http.post(`/api/administratorControllerServer/postManagement/queryCoreStationByConditionOnMultiplePages`, obj),
  // 删除
  deleteObj:(id) => http.post(`/api/administratorControllerServer/postManagement/delCoreStationAllInfo`, id),
  // 添加
  insertObj:(obj) => http.post(`/api/administratorControllerServer/postManagement/addCoreStationAllInfo`, obj),
  // 修改
  updateObj:(obj) => http.post(`/api/administratorControllerServer/postManagement/updateCoreStationAllInfo`, obj),
}
