import http from '../../../utils/http'

let url = '/api/administratorControllerServer'
export default {
  //分页查询
  selRoleAll: (roles) => http.post(`${url}/roleManagement/pagingQueryRolesBasedOnAssemblyCriteria`, roles),
  //删除多个
  delByIds: (list) => http.post(`${url}/roleManagement/pagingDelRolesBasedOnAssemblyCriteria`, list),
  //status查询组织
  selOrgAll: (status) => http.post(`${url}/organizationManagement/queryCoreOrgAllInfo`,status),
  //查询所有组织
  selectOrgAll: () => http.post(`${url}/organizationManagement/queryCoreOrgAllInfoNo`),
  //角色授权
  shouquan: (list) => http.post(`${url}/roleManagement/authorizationIsBasedOnTheUserIDSet`,list),
  //修改角色
  updateRole: (dto) => http.post(`${url}/roleManagement/pagingUpdateRolesBasedOnAssemblyCriteria`,dto),
  //添加角色
  addRole: (dto) => http.post(`${url}/roleManagement/pagingAddRolesBasedOnAssemblyCriteria`,dto),
  //菜单配置
  getMenu :(status) => http.post( `${url}/pdAuthMenu/menuAll`,status),
  //查询角色已有菜单权限
  getMenuQuanxian: (roleId) => http.get(`${url}/roleManagement/queryValidPermissionsBasedOnTheRoleID/${roleId}`),
  //菜单ID寻找权限
  getMenuId: (menuId) => http.get(`${url}/menuManagement/queryMenuButtonsByMenuId/${menuId}`),
  //给角色添加菜单配置
  peizhiMenu: (dto) => http.post(`${url}/roleManagement/configureTheMenuBasedOnTheMenuIDSetAndButtonIDSetRoleID`,dto)
}
