import http from "../../../utils/http";

let httpHead = '/api/administratorControllerServer/'
export default {
  // 获取数据
  getTreeData: (name) => http.post(`${httpHead}/organizationManagement/queryCoreOrgAllInfo`, {'name': name}),

  // 删除
  deleteData: (idList) => http.post(`${httpHead}/organizationManagement/delCoreOrgAllInfo`, idList),

  // 获取行政地址
  getPdAreaTree: () => http.get('/api/userControllerServer/pdArea/getPdAreaTree'),

  // 修改
  updateAggData: (param) => http.post(`${httpHead}/organizationManagement/updateCoreOrgAllInfo`, param),

  // 添加
  insertAggdata: (param) => http.post(`${httpHead}/organizationManagement/addCoreOrgAllInfo`, param)
}
