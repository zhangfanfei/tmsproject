import http from '../../../utils/http'

let url = '/api/administratorControllerServer'
export default {
  // 查询菜单
  getMenuAll:(obj) => http.post(`/api/administratorControllerServer/menuManagement/menuAll`,obj),
  // 添加
  insertMenuAll:(obj) => http.post(`/api/administratorControllerServer/menuManagement/addMenu`,obj),
  // 修改
  updateMenuAll:(obj) => http.post(`/api/administratorControllerServer/menuManagement/updateMenu`,obj),
  // 删除
  deleteMenuAll:(id) => http.post(`/api/administratorControllerServer/menuManagement/delMenuByIdList`,{"idList":id}),
  // 查询菜单相关数据
  getMenuButtons:(obj) => http.post(`/api/administratorControllerServer/menuManagement/queryMenuButtonsByCriteria`,obj),
  // 添加权限
  insertMenuButtons:(obj) => http.post(`/api/administratorControllerServer/menuManagement/addMenuButtonsByCriteria`,obj),
  // 修改权限
  updateMenuButtons:(obj) => http.post(`/api/administratorControllerServer/menuManagement/updateMenuButtonsByCriteria`,obj),
  // 删除权限
  deleteMenuButtons:(obj) => http.post(`/api/administratorControllerServer/menuManagement/delMenuButtonsByCriteria`,obj)

}



