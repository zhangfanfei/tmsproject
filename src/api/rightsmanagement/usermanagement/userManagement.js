import http from "../../../utils/http";

let url = '/api/administratorControllerServer/'
let httpHead = '/api/administratorControllerServer/'
export default {
  //角色ID查询用户
  selUserAll: (id) => http.get(`${url}userManagement/exampleQueryTheUserAssociationOfARole/${id}`),

  // 查询所有用户
  sellUserData: (param) => http.post(`${httpHead}userManagement/queryUserInformationOnMultiplePages`, param),

  // 删除
  deleteUserInfo: (uid) => http.post(`${httpHead}userManagement/deleteUserInformationBasedOnTheUserIDSet`, uid),

  // 获取岗位
  getPdCoreOrg: (id) => http.get(`${httpHead}/postManagement/queryCoreStationByCoreOrgId/${id}`),

  // 修改
  updateUserData: (userData) => http.post(`${httpHead}/userManagement/updateingUserInformation`, userData),

  // 添加
  insertUserData: (userData) => http.post(`${httpHead}/userManagement/addingUserInformation`, userData)
}



