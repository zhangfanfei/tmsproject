import http from '../../../utils/http'

let url = '/api/administratorControllerServer'
export default {
  selLogAll: () => http.post(`${url}/pdloginUser/queryLoginLogByLoginId`),

  getUser: () => http.post(`${url}/pdloginUser/queryUserinfoByLoginId`),

  updateUser: (user)=> http.post(`${url}/pdloginUser/updateUserinfoByLoginId`,user),

  updatePassword: (password)=>http.post(`${url}/pdloginUser/changingYourPassword`,password)


}
