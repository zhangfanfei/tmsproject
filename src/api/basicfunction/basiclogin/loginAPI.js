import http from '../../../utils/http'

let url = `/api/administratorControllerServer`
export default {
  // 登录
  login: params =>  http.post( `${url}/pdAuthUser/login`,params),
  //数据面板
  getData: () => http.post(`${url}/panelData/queryPanelDataByLoginId`),
  // 获取角色ww
  getRoles: () => http.post(`${url}/pdAuthUser/getUserRole`),
}

