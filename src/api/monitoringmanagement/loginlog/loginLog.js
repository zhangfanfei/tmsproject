import http from '../../../utils/http'

let url = '/api/administratorControllerServer'
export default {
  selLogAll: (currentPage, pageSize, logs) => http.post(`${url}/pdCommonLoginLog/logs/${currentPage}/${pageSize}`, logs),

  delById: (id) => http.delete(`${url}/pdCommonLoginLog/del/${id}`),

  delByIds: (list) => http.delete(`${url}/pdCommonLoginLog/dels`,list)
}
