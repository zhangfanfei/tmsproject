import http from '../../../utils/http'

let url = '/api/administratorControllerServer'
export default {
  operaLog: (currentPage, pageSize, logs) => http.post(`${url}/pdAuthLog/opLogs/${currentPage}/${pageSize}`, logs),

  delById: (id) => http.delete(`${url}/pdAuthLog/del/${id}`),

  delByIds: (list) => http.delete(`${url}/pdAuthLog/dels`,list)
}
