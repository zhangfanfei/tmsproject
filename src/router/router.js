// 导入组件
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/store';

//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  // 清空点击首页后的面包屑
  store.commit("selectMenu", [])
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter);
export default new VueRouter({
  // mode: 'history',
  routes: [
    {
      title: '登录',
      path: '',
      name: 'losins',
      component: (resolve) => require(['../main/login'], resolve)
    },
    {
      title: '登录',
      path: '/login',
      name: 'login',
      component: (resolve) => require(['../main/login'], resolve)
    },
    {
      path: '/home',
      name: 'home',
      title: '桌面',
      component: (resolve) => require(['../main/home'], resolve),
      children: [
        {
          title: '首页',
          path: '/index',
          name: 'index',
          component: (resolve) => require(['../main/index'], resolve),
          children: [
            {
              title: '饼状图1',
              path: 'pieChartFirst',
              name: 'pieChartFirst',
              component: (resolve) => require(['../components/Chart/pie/pieChart1'], resolve),
            },
            {
              title: '饼状图2',
              path: 'pieChartSecond',
              name: 'pieChartSecond',
              component: (resolve) => require(['../components/Chart/pie/pieChart2'], resolve),
            },
            {
              title: '饼状图3',
              path: 'pieChartThird',
              name: 'pieChartThird',
              component: (resolve) => require(['../components/Chart/pie/pieChart3'], resolve),
            },
          ]
        },
        {
          title: '个人中心',
          name: 'myCenter',
          path: '/myCenter',
          component: (resolve) => require(['../main/myCenter'], resolve)
        },
        {
          title: '订单列表',
          path: '/orderList',
          name: 'orderList',
          component: (resolve) => require(['../components/LogisticsManagement/orderManagement/orderList'], resolve)
        },
        {
          title: '订单详情',
          path: '/xiangqing',
          name: 'xiangqing',
          component: (resolve) => require(['../components/LogisticsManagement/orderManagement/xiangqing'], resolve)
        },
        {
          title: '运单列表',
          path: '/orderCargoList',
          name: 'orderCargoList',
          component: (resolve) => require(['../components/LogisticsManagement/orderCargoManagement/orderCargoList'], resolve)
        },
        {
          title: '操作日志',
          path: '/operaLog',
          name: 'operaLog',
          component: (resolve) => require(['../components/MonitoringManagement/operaLog'], resolve)
        },
        {
          title: '登录日志',
          name: 'loginLog',
          path: '/loginLog',
          component: (resolve) => require(['../components/MonitoringManagement/loginLog'], resolve)
        },
        {
          title: '运单轨迹',
          path: '/orderCargoTrack',
          name: 'orderCargoTrack',
          component: (resolve) => require(['../components/LogisticsManagement/orderCargoManagement/orderCargoTrack'], resolve)
        },
        {
          title: '快递作业管理',
          name: 'operationalManagement',
          path: '/operationalManagement',
          component: (resolve) => require(['../components/LogisticsManagement/branchManagement/operationalManagement'], resolve)
        },
        {
          title: '货品管理',
          path: '/itemType',
          name: 'itemType',
          component: (resolve) => require(['../components/LogisticsManagement/branchManagement/itemType'], resolve)
        },
        {
          title: '车型管理',
          path: '/carManagement',
          name: 'carManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/carManagement'], resolve)
        },
        {
          title: '车队管理',
          path: '/carTeam',
          name: 'carTeam',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/carTeam'], resolve)
        },
        {
          title: '车队里的车辆列表',
          path: '/truckliebiao',
          name: 'truckliebiao',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/truckliebiao'], resolve)
        },
        {
          title: '车队里的司机列表',
          path: '/driverliebiao',
          name: 'driverliebiao',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/driverliebiao'], resolve)
        },
        {
          title: '车辆管理',
          path: '/vehicleManagement',
          name: 'vehicleManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/vehicleManagement'], resolve)
        },
        {
          title: '车辆详情',
          path: '/truckDetails',
          name: 'truckDetails',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/truckDetails'], resolve)
        },
        {
          title: '司机管理',
          path: '/driverManagement',
          name: 'driverManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/driverManagement'], resolve)
        },
        {
          title: '司机详情',
          path: '/driverDetails',
          name: 'driverDetails',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/driverDetails'], resolve)
        },
        {
          title: '线路管理',
          path: '/lineManagement',
          name: 'lineManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/lineManagement'], resolve)
        },
        {
          title: '线路类型管理',
          path: '/lineTypeManagement',
          name: 'lineTypeManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transferCenter/lineTypeManagement'], resolve)
        },
        {
          title: '智能调度',
          path: '/intelligentScheduling',
          name: 'intelligentScheduling',
          component: (resolve) => require(['../components/LogisticsManagement/dispatchingManagement/intelligentScheduling'], resolve)
        },
        {
          title: '调度信息',
          path: '/schedulingInformation',
          name: 'schedulingInformation',
          component: (resolve) => require(['../components/LogisticsManagement/dispatchingManagement/schedulingInformation'], resolve)
        },
        {
          title: '角色管理',
          path: '/roleManagement',
          name: 'roleManagement',
          component: (resolve) => require(['../components/SystemManagement/roleManagement'], resolve)
        },
        {
          title: '菜单配置',
          path: '/configurationMenu',
          name: 'configurationMenu',
          component: (resolve) => require(['../components/SystemManagement/configurationMenu'], resolve)
        },
        {
          title: '司机作业单管理',
          path: '/driverOrderManagement',
          name: 'driverOrderManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transportManagemenet/driverOrderManagement'], resolve)
        },
        {
          title: '运输任务管理',
          path: '/transportTaskManagement',
          name: 'transportTaskManagement',
          component: (resolve) => require(['../components/LogisticsManagement/transportManagemenet/transportTaskManagement'], resolve)
        },
        {
          title: '运输任务管理(查看任务详情)',
          path: '/truckInfo',
          name: 'truckInfo',
          component: (resolve) => require(['../components/LogisticsManagement/transportManagemenet/transportTaskManagementChile/truckInfo'], resolve)
        },
        {
          title: '快递员作业范围管理',
          path: '/operationalAreaManagement',
          name: 'operationalAreaManagement',
          component: (resolve) => require(['../components/LogisticsManagement/branchManagement/operationalAreaManagement'], resolve)
        },
        {
          title: '快递员作业范围详情',
          path: '/operationalAreaUpdate',
          name: 'operationalAreaUpdate',
          component: (resolve) => require(['../components/LogisticsManagement/branchManagement/operationalAreaUpdate'], resolve)
        },
        {
          title: '机构作业范围管理',
          path: '/organizationScope',
          name: 'organizationScope',
          component: (resolve) => require(['../components/LogisticsManagement/organization/organizationScope'], resolve)
        },
        {
          title: '快递员详情',
          path: '/operationalAreaManagementXQ',
          name: 'operationalAreaManagementXQ',
          component: (resolve) => require(['../components/LogisticsManagement/branchManagement/operationalAreaManagementXQ'], resolve)
        },
        {
          title: '运单详情',
          path: '/orderCargoXQ',
          name: 'orderCargoXQ',
          component: (resolve) => require(['../components/LogisticsManagement/orderCargoManagement/orderCargoXQ'], resolve)
        },
        {
          title: '无权访问',
          path: '/noAccess',
          name: 'noAccess',
          component: (resolve) => require(['../components/error/noAccess'], resolve)
        },
        {
          title: '资源不存在',
          path: '/noPage',
          name: 'noPage',
          component: (resolve) => require(['../components/error/noPage'], resolve)
        },
        {
          title: '组织管理',
          path: '/organizationManagement',
          name: 'organizationManagement',
          component: (resolve) => require(['../components/SystemManagement/organizationManagement'], resolve)
        },
        {
          title: '岗位管理',
          path: '/jobManagement',
          name: 'jobManagement',
          component: (resolve) => require(['../components/SystemManagement/jobManagement'], resolve)
        },
        {
          title: '用户管理',
          path: '/userManagement',
          name: 'userManagement',
          component: (resolve) => require(['../components/SystemManagement/userManagement'], resolve)
        }
      ],
      redirect: '/index/pieChartFirst' // 进入home默认重定向到，首页index下的饼状图
    }
  ]
})
