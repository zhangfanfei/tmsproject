import Vue from 'vue';
import Vuex from 'vuex';
import {delCookie, setCookie} from "../utils/util";
import {Loading} from 'element-ui';

Vue.use(Vuex);
// 登录验证
export default new Vuex.Store({
  state: {
    //当前菜单
    currentMenu: null,
    token: null,
    color: ['#325B69', '#698570', '#AE5548', '#6D9EA8', '#9CC2B0', '#C98769'],
    // oss服务器地址
    ossaddr: 'https://java-xiaozhang.oss-cn-shenzhen.aliyuncs.com',
    animationStatus: null
  },
  getters: {
    isLogin(state) {
      return state.token != null;
    }
  },
  mutations: {
    // 启动动画
    startAnimation(state, status) {
      if (status) {
        state.animationStatus = Loading.service({
          lock: true,
          text: '正在加载中~~~',
          spinner: 'el-icon-loading',
          background: 'rgba(255, 255, 255, 0.7)',
        });
      } else {
        if (state.animationStatus != null)
          state.animationStatus.close() // 停止动画
      }
    },
    // 设置面包屑的参数
    selectMenu(state, menuList) {
      state.currentMenu = menuList
    },
    // 登录
    login(state, data) {
      state.token = data.token
      setCookie('token', state.token, 1)
    },
    // 退出
    logout(state) {
      state.role = {}
      delCookie('token')
    }
  }
})
